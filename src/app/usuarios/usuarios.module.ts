import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { TablaUsuariosComponent } from './tabla-usuarios/tabla-usuarios.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    TablaUsuariosComponent
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    UsuariosRoutingModule
  ],
  exports: [
    TablaUsuariosComponent
  ]
})
export class UsuariosModule { }
