import { HttpClient } from '@angular/common/http';
import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-tabla-usuarios',
  templateUrl: './tabla-usuarios.component.html',
  styleUrls: ['./tabla-usuarios.component.css']

})
export class TablaUsuariosComponent implements OnInit{

  constructor(private http: HttpClient) { console.log('agarrado') }
  usuarios:any = [];
  ngOnInit() {

    this.getUsuarios().subscribe( {
      next:(response) => {
        this.usuarios = response;
      },
      error:() => {

      },
    })
  }
  getUsuarios(){
   return this.http.get('http://capi_examen_back_carlos_mar.test/api/users')
  //  .pipe(map())
  }



}
